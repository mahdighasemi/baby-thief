gdjs.MainMenuCode = {};
gdjs.MainMenuCode.GDMainMenu_95BGObjects1= [];
gdjs.MainMenuCode.GDMainMenu_95BGObjects2= [];
gdjs.MainMenuCode.GDMainMenu_95BGObjects3= [];
gdjs.MainMenuCode.GDOPTION_95TEXTObjects1= [];
gdjs.MainMenuCode.GDOPTION_95TEXTObjects2= [];
gdjs.MainMenuCode.GDOPTION_95TEXTObjects3= [];
gdjs.MainMenuCode.GDQUIT_95TEXTObjects1= [];
gdjs.MainMenuCode.GDQUIT_95TEXTObjects2= [];
gdjs.MainMenuCode.GDQUIT_95TEXTObjects3= [];
gdjs.MainMenuCode.GDSTARTGAME_95TEXTObjects1= [];
gdjs.MainMenuCode.GDSTARTGAME_95TEXTObjects2= [];
gdjs.MainMenuCode.GDSTARTGAME_95TEXTObjects3= [];
gdjs.MainMenuCode.GDLOGO_95GAMEObjects1= [];
gdjs.MainMenuCode.GDLOGO_95GAMEObjects2= [];
gdjs.MainMenuCode.GDLOGO_95GAMEObjects3= [];
gdjs.MainMenuCode.GDQuit_95BTNObjects1= [];
gdjs.MainMenuCode.GDQuit_95BTNObjects2= [];
gdjs.MainMenuCode.GDQuit_95BTNObjects3= [];
gdjs.MainMenuCode.GDOption_95BTNObjects1= [];
gdjs.MainMenuCode.GDOption_95BTNObjects2= [];
gdjs.MainMenuCode.GDOption_95BTNObjects3= [];
gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1= [];
gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects2= [];
gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects3= [];
gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1= [];
gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects2= [];
gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects3= [];
gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1= [];
gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects2= [];
gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects3= [];

gdjs.MainMenuCode.conditionTrue_0 = {val:false};
gdjs.MainMenuCode.condition0IsTrue_0 = {val:false};
gdjs.MainMenuCode.condition1IsTrue_0 = {val:false};
gdjs.MainMenuCode.conditionTrue_1 = {val:false};
gdjs.MainMenuCode.condition0IsTrue_1 = {val:false};
gdjs.MainMenuCode.condition1IsTrue_1 = {val:false};


gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDSTART_9595Game_9595BTNObjects1Objects = Hashtable.newFrom({"START_Game_BTN": gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1});gdjs.MainMenuCode.eventsList0 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Wood-Tap_1.ogg", false, 100, 1);
}}

}


};gdjs.MainMenuCode.eventsList1 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Zoop_1.ogg", false, 100, 1);
}}

}


};gdjs.MainMenuCode.eventsList2 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
{gdjs.MainMenuCode.conditionTrue_1 = gdjs.MainMenuCode.condition0IsTrue_0;
gdjs.MainMenuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56543324);
}
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainMenuCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.MainMenuCode.eventsList3 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
{gdjs.MainMenuCode.conditionTrue_1 = gdjs.MainMenuCode.condition0IsTrue_0;
gdjs.MainMenuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56541492);
}
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainMenuCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Cutscene", false);
}
{ //Subevents
gdjs.MainMenuCode.eventsList2(runtimeScene);} //End of subevents
}

}


};gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDOption_9595BTNObjects1Objects = Hashtable.newFrom({"Option_BTN": gdjs.MainMenuCode.GDOption_95BTNObjects1});gdjs.MainMenuCode.eventsList4 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Wood-Tap_1.ogg", false, 100, 1);
}}

}


};gdjs.MainMenuCode.eventsList5 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Zoop_1.ogg", false, 100, 1);
}}

}


};gdjs.MainMenuCode.eventsList6 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
{gdjs.MainMenuCode.conditionTrue_1 = gdjs.MainMenuCode.condition0IsTrue_0;
gdjs.MainMenuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56546908);
}
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainMenuCode.eventsList5(runtimeScene);} //End of subevents
}

}


};gdjs.MainMenuCode.eventsList7 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
{gdjs.MainMenuCode.conditionTrue_1 = gdjs.MainMenuCode.condition0IsTrue_0;
gdjs.MainMenuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56545044);
}
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainMenuCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Option", false);
}
{ //Subevents
gdjs.MainMenuCode.eventsList6(runtimeScene);} //End of subevents
}

}


};gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDQuit_9595BTNObjects1Objects = Hashtable.newFrom({"Quit_BTN": gdjs.MainMenuCode.GDQuit_95BTNObjects1});gdjs.MainMenuCode.eventsList8 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Wood-Tap_1.ogg", false, 100, 1);
}}

}


};gdjs.MainMenuCode.eventsList9 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Zoop_1.ogg", false, 100, 1);
}}

}


};gdjs.MainMenuCode.eventsList10 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
{gdjs.MainMenuCode.conditionTrue_1 = gdjs.MainMenuCode.condition0IsTrue_0;
gdjs.MainMenuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56550428);
}
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainMenuCode.eventsList9(runtimeScene);} //End of subevents
}

}


};gdjs.MainMenuCode.eventsList11 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
{gdjs.MainMenuCode.conditionTrue_1 = gdjs.MainMenuCode.condition0IsTrue_0;
gdjs.MainMenuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56548644);
}
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainMenuCode.eventsList8(runtimeScene);} //End of subevents
}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.stopGame(runtimeScene);
}
{ //Subevents
gdjs.MainMenuCode.eventsList10(runtimeScene);} //End of subevents
}

}


};gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDOption_9595BTNObjects1Objects = Hashtable.newFrom({"Option_BTN": gdjs.MainMenuCode.GDOption_95BTNObjects1});gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDSTART_9595Game_9595BTNObjects1Objects = Hashtable.newFrom({"START_Game_BTN": gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1});gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDQuit_9595BTNObjects1Objects = Hashtable.newFrom({"Quit_BTN": gdjs.MainMenuCode.GDQuit_95BTNObjects1});gdjs.MainMenuCode.eventsList12 = function(runtimeScene) {

{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 1;
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "01 Start Menu.ogg", 1, false, 100, 1);
}}

}


};gdjs.MainMenuCode.eventsList13 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("START_Game_BTN"), gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1);

gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDSTART_9595Game_9595BTNObjects1Objects, runtimeScene, true, false);
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1 */
{for(var i = 0, len = gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.MainMenuCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Option_BTN"), gdjs.MainMenuCode.GDOption_95BTNObjects1);

gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDOption_9595BTNObjects1Objects, runtimeScene, true, false);
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainMenuCode.GDOption_95BTNObjects1 */
{for(var i = 0, len = gdjs.MainMenuCode.GDOption_95BTNObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDOption_95BTNObjects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.MainMenuCode.eventsList7(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Quit_BTN"), gdjs.MainMenuCode.GDQuit_95BTNObjects1);

gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDQuit_9595BTNObjects1Objects, runtimeScene, true, false);
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainMenuCode.GDQuit_95BTNObjects1 */
{for(var i = 0, len = gdjs.MainMenuCode.GDQuit_95BTNObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDQuit_95BTNObjects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.MainMenuCode.eventsList11(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Option_BTN"), gdjs.MainMenuCode.GDOption_95BTNObjects1);

gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDOption_9595BTNObjects1Objects, runtimeScene, true, true);
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainMenuCode.GDOption_95BTNObjects1 */
{for(var i = 0, len = gdjs.MainMenuCode.GDOption_95BTNObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDOption_95BTNObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("START_Game_BTN"), gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1);

gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDSTART_9595Game_9595BTNObjects1Objects, runtimeScene, true, true);
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1 */
{for(var i = 0, len = gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Quit_BTN"), gdjs.MainMenuCode.GDQuit_95BTNObjects1);

gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainMenuCode.mapOfGDgdjs_46MainMenuCode_46GDQuit_9595BTNObjects1Objects, runtimeScene, true, true);
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainMenuCode.GDQuit_95BTNObjects1 */
{for(var i = 0, len = gdjs.MainMenuCode.GDQuit_95BTNObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDQuit_95BTNObjects1[i].setAnimation(0);
}
}}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.MainMenuCode.eventsList12(runtimeScene);} //End of subevents
}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtsExt__Gamepads__C_Button_pressed.func(runtimeScene, 1, "START", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Cutscene", false);
}}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtsExt__Gamepads__C_Button_pressed.func(runtimeScene, 1, "BACK", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.stopGame(runtimeScene);
}}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = gdjs.evtsExt__Gamepads__C_Controller_type.func(runtimeScene, 1, "Xbox", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("CONTROLLER_BACK_BUTTON"), gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1);
gdjs.copyArray(runtimeScene.getObjects("CONTROLLER_START_BUTTON"), gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1);
{for(var i = 0, len = gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1[i].hide(false);
}
}}

}


{


gdjs.MainMenuCode.condition0IsTrue_0.val = false;
{
gdjs.MainMenuCode.condition0IsTrue_0.val = !(gdjs.evtsExt__Gamepads__C_Controller_type.func(runtimeScene, 1, "Xbox", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}if (gdjs.MainMenuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("CONTROLLER_BACK_BUTTON"), gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1);
gdjs.copyArray(runtimeScene.getObjects("CONTROLLER_START_BUTTON"), gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1);
{for(var i = 0, len = gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1.length ;i < len;++i) {
    gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1[i].hide();
}
}}

}


};

gdjs.MainMenuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MainMenuCode.GDMainMenu_95BGObjects1.length = 0;
gdjs.MainMenuCode.GDMainMenu_95BGObjects2.length = 0;
gdjs.MainMenuCode.GDMainMenu_95BGObjects3.length = 0;
gdjs.MainMenuCode.GDOPTION_95TEXTObjects1.length = 0;
gdjs.MainMenuCode.GDOPTION_95TEXTObjects2.length = 0;
gdjs.MainMenuCode.GDOPTION_95TEXTObjects3.length = 0;
gdjs.MainMenuCode.GDQUIT_95TEXTObjects1.length = 0;
gdjs.MainMenuCode.GDQUIT_95TEXTObjects2.length = 0;
gdjs.MainMenuCode.GDQUIT_95TEXTObjects3.length = 0;
gdjs.MainMenuCode.GDSTARTGAME_95TEXTObjects1.length = 0;
gdjs.MainMenuCode.GDSTARTGAME_95TEXTObjects2.length = 0;
gdjs.MainMenuCode.GDSTARTGAME_95TEXTObjects3.length = 0;
gdjs.MainMenuCode.GDLOGO_95GAMEObjects1.length = 0;
gdjs.MainMenuCode.GDLOGO_95GAMEObjects2.length = 0;
gdjs.MainMenuCode.GDLOGO_95GAMEObjects3.length = 0;
gdjs.MainMenuCode.GDQuit_95BTNObjects1.length = 0;
gdjs.MainMenuCode.GDQuit_95BTNObjects2.length = 0;
gdjs.MainMenuCode.GDQuit_95BTNObjects3.length = 0;
gdjs.MainMenuCode.GDOption_95BTNObjects1.length = 0;
gdjs.MainMenuCode.GDOption_95BTNObjects2.length = 0;
gdjs.MainMenuCode.GDOption_95BTNObjects3.length = 0;
gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects1.length = 0;
gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects2.length = 0;
gdjs.MainMenuCode.GDSTART_95Game_95BTNObjects3.length = 0;
gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects1.length = 0;
gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects2.length = 0;
gdjs.MainMenuCode.GDCONTROLLER_95START_95BUTTONObjects3.length = 0;
gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects1.length = 0;
gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects2.length = 0;
gdjs.MainMenuCode.GDCONTROLLER_95BACK_95BUTTONObjects3.length = 0;

gdjs.MainMenuCode.eventsList13(runtimeScene);
return;

}

gdjs['MainMenuCode'] = gdjs.MainMenuCode;
