gdjs.CutsceneCode = {};
gdjs.CutsceneCode.GDMainMenu_95BGObjects1= [];
gdjs.CutsceneCode.GDMainMenu_95BGObjects2= [];
gdjs.CutsceneCode.GDMainMenu_95BGObjects3= [];
gdjs.CutsceneCode.GDMainMenu_95BGObjects4= [];
gdjs.CutsceneCode.GDOPTION_95TEXTObjects1= [];
gdjs.CutsceneCode.GDOPTION_95TEXTObjects2= [];
gdjs.CutsceneCode.GDOPTION_95TEXTObjects3= [];
gdjs.CutsceneCode.GDOPTION_95TEXTObjects4= [];
gdjs.CutsceneCode.GDQUIT_95TEXTObjects1= [];
gdjs.CutsceneCode.GDQUIT_95TEXTObjects2= [];
gdjs.CutsceneCode.GDQUIT_95TEXTObjects3= [];
gdjs.CutsceneCode.GDQUIT_95TEXTObjects4= [];
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects1= [];
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects2= [];
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects3= [];
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects4= [];
gdjs.CutsceneCode.GDLOGO_95GAMEObjects1= [];
gdjs.CutsceneCode.GDLOGO_95GAMEObjects2= [];
gdjs.CutsceneCode.GDLOGO_95GAMEObjects3= [];
gdjs.CutsceneCode.GDLOGO_95GAMEObjects4= [];
gdjs.CutsceneCode.GDQuit_95BTNObjects1= [];
gdjs.CutsceneCode.GDQuit_95BTNObjects2= [];
gdjs.CutsceneCode.GDQuit_95BTNObjects3= [];
gdjs.CutsceneCode.GDQuit_95BTNObjects4= [];
gdjs.CutsceneCode.GDCutsceneObjects1= [];
gdjs.CutsceneCode.GDCutsceneObjects2= [];
gdjs.CutsceneCode.GDCutsceneObjects3= [];
gdjs.CutsceneCode.GDCutsceneObjects4= [];

gdjs.CutsceneCode.conditionTrue_0 = {val:false};
gdjs.CutsceneCode.condition0IsTrue_0 = {val:false};
gdjs.CutsceneCode.condition1IsTrue_0 = {val:false};
gdjs.CutsceneCode.conditionTrue_1 = {val:false};
gdjs.CutsceneCode.condition0IsTrue_1 = {val:false};
gdjs.CutsceneCode.condition1IsTrue_1 = {val:false};


gdjs.CutsceneCode.mapOfGDgdjs_46CutsceneCode_46GDQuit_9595BTNObjects2Objects = Hashtable.newFrom({"Quit_BTN": gdjs.CutsceneCode.GDQuit_95BTNObjects2});gdjs.CutsceneCode.eventsList0 = function(runtimeScene) {

{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Wood-Tap_1.ogg", false, 100, 1);
}}

}


};gdjs.CutsceneCode.eventsList1 = function(runtimeScene) {

{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "Zoop_1.ogg", false, 100, 1);
}}

}


};gdjs.CutsceneCode.eventsList2 = function(runtimeScene) {

{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
{gdjs.CutsceneCode.conditionTrue_1 = gdjs.CutsceneCode.condition0IsTrue_0;
gdjs.CutsceneCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56611244);
}
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.CutsceneCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.CutsceneCode.eventsList3 = function(runtimeScene) {

{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
{gdjs.CutsceneCode.conditionTrue_1 = gdjs.CutsceneCode.condition0IsTrue_0;
gdjs.CutsceneCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(56609460);
}
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.CutsceneCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.stopGame(runtimeScene);
}
{ //Subevents
gdjs.CutsceneCode.eventsList2(runtimeScene);} //End of subevents
}

}


};gdjs.CutsceneCode.mapOfGDgdjs_46CutsceneCode_46GDQuit_9595BTNObjects1Objects = Hashtable.newFrom({"Quit_BTN": gdjs.CutsceneCode.GDQuit_95BTNObjects1});gdjs.CutsceneCode.eventsList4 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.CutsceneCode.GDQuit_95BTNObjects1, gdjs.CutsceneCode.GDQuit_95BTNObjects2);


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CutsceneCode.mapOfGDgdjs_46CutsceneCode_46GDQuit_9595BTNObjects2Objects, runtimeScene, true, false);
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CutsceneCode.GDQuit_95BTNObjects2 */
{for(var i = 0, len = gdjs.CutsceneCode.GDQuit_95BTNObjects2.length ;i < len;++i) {
    gdjs.CutsceneCode.GDQuit_95BTNObjects2[i].setAnimation(1);
}
}
{ //Subevents
gdjs.CutsceneCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.CutsceneCode.GDQuit_95BTNObjects1 */

gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CutsceneCode.mapOfGDgdjs_46CutsceneCode_46GDQuit_9595BTNObjects1Objects, runtimeScene, true, true);
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
/* Reuse gdjs.CutsceneCode.GDQuit_95BTNObjects1 */
{for(var i = 0, len = gdjs.CutsceneCode.GDQuit_95BTNObjects1.length ;i < len;++i) {
    gdjs.CutsceneCode.GDQuit_95BTNObjects1[i].setAnimation(0);
}
}}

}


};gdjs.CutsceneCode.eventsList5 = function(runtimeScene) {

{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = !(gdjs.evtTools.systemInfo.isWebGLSupported(runtimeScene));
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("QUIT_TEXT"), gdjs.CutsceneCode.GDQUIT_95TEXTObjects1);
gdjs.copyArray(runtimeScene.getObjects("Quit_BTN"), gdjs.CutsceneCode.GDQuit_95BTNObjects1);
{for(var i = 0, len = gdjs.CutsceneCode.GDQuit_95BTNObjects1.length ;i < len;++i) {
    gdjs.CutsceneCode.GDQuit_95BTNObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.CutsceneCode.GDQUIT_95TEXTObjects1.length ;i < len;++i) {
    gdjs.CutsceneCode.GDQUIT_95TEXTObjects1[i].hide(false);
}
}
{ //Subevents
gdjs.CutsceneCode.eventsList4(runtimeScene);} //End of subevents
}

}


};gdjs.CutsceneCode.eventsList6 = function(runtimeScene) {

{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isWebGLSupported(runtimeScene);
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("QUIT_TEXT"), gdjs.CutsceneCode.GDQUIT_95TEXTObjects1);
gdjs.copyArray(runtimeScene.getObjects("Quit_BTN"), gdjs.CutsceneCode.GDQuit_95BTNObjects1);
{for(var i = 0, len = gdjs.CutsceneCode.GDQuit_95BTNObjects1.length ;i < len;++i) {
    gdjs.CutsceneCode.GDQuit_95BTNObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.CutsceneCode.GDQUIT_95TEXTObjects1.length ;i < len;++i) {
    gdjs.CutsceneCode.GDQUIT_95TEXTObjects1[i].hide();
}
}}

}


};gdjs.CutsceneCode.eventsList7 = function(runtimeScene) {

{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.CutsceneCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
gdjs.CutsceneCode.condition0IsTrue_0.val = gdjs.evtTools.systemInfo.isMobile();
}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.CutsceneCode.eventsList6(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("Cutscene"), gdjs.CutsceneCode.GDCutsceneObjects1);

gdjs.CutsceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CutsceneCode.GDCutsceneObjects1.length;i<l;++i) {
    if ( gdjs.CutsceneCode.GDCutsceneObjects1[i].hasAnimationEnded() ) {
        gdjs.CutsceneCode.condition0IsTrue_0.val = true;
        gdjs.CutsceneCode.GDCutsceneObjects1[k] = gdjs.CutsceneCode.GDCutsceneObjects1[i];
        ++k;
    }
}
gdjs.CutsceneCode.GDCutsceneObjects1.length = k;}if (gdjs.CutsceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level 1-1", false);
}{runtimeScene.getGame().getVariables().getFromIndex(9).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(10).setNumber(3);
}}

}


};

gdjs.CutsceneCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.CutsceneCode.GDMainMenu_95BGObjects1.length = 0;
gdjs.CutsceneCode.GDMainMenu_95BGObjects2.length = 0;
gdjs.CutsceneCode.GDMainMenu_95BGObjects3.length = 0;
gdjs.CutsceneCode.GDMainMenu_95BGObjects4.length = 0;
gdjs.CutsceneCode.GDOPTION_95TEXTObjects1.length = 0;
gdjs.CutsceneCode.GDOPTION_95TEXTObjects2.length = 0;
gdjs.CutsceneCode.GDOPTION_95TEXTObjects3.length = 0;
gdjs.CutsceneCode.GDOPTION_95TEXTObjects4.length = 0;
gdjs.CutsceneCode.GDQUIT_95TEXTObjects1.length = 0;
gdjs.CutsceneCode.GDQUIT_95TEXTObjects2.length = 0;
gdjs.CutsceneCode.GDQUIT_95TEXTObjects3.length = 0;
gdjs.CutsceneCode.GDQUIT_95TEXTObjects4.length = 0;
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects1.length = 0;
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects2.length = 0;
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects3.length = 0;
gdjs.CutsceneCode.GDSTARTGAME_95TEXTObjects4.length = 0;
gdjs.CutsceneCode.GDLOGO_95GAMEObjects1.length = 0;
gdjs.CutsceneCode.GDLOGO_95GAMEObjects2.length = 0;
gdjs.CutsceneCode.GDLOGO_95GAMEObjects3.length = 0;
gdjs.CutsceneCode.GDLOGO_95GAMEObjects4.length = 0;
gdjs.CutsceneCode.GDQuit_95BTNObjects1.length = 0;
gdjs.CutsceneCode.GDQuit_95BTNObjects2.length = 0;
gdjs.CutsceneCode.GDQuit_95BTNObjects3.length = 0;
gdjs.CutsceneCode.GDQuit_95BTNObjects4.length = 0;
gdjs.CutsceneCode.GDCutsceneObjects1.length = 0;
gdjs.CutsceneCode.GDCutsceneObjects2.length = 0;
gdjs.CutsceneCode.GDCutsceneObjects3.length = 0;
gdjs.CutsceneCode.GDCutsceneObjects4.length = 0;

gdjs.CutsceneCode.eventsList7(runtimeScene);
return;

}

gdjs['CutsceneCode'] = gdjs.CutsceneCode;
